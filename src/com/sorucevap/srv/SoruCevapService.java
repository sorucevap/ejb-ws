package com.sorucevap.srv;

import java.util.Collection;
import java.util.List;

import javax.ejb.Remote;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.sorucevap.srv.entity.Answer;
import com.sorucevap.srv.entity.Category;
import com.sorucevap.srv.entity.Question;
import com.sorucevap.srv.entity.User;

@Remote
public interface SoruCevapService
{
	public static final String RemoteJNDIName = "SoruCevapServiceBean/remote";
	
	public <T> T save( T o);
	
	public void remove( Object o);
	
	public Object refresh( Object o);
	
	@SuppressWarnings( "rawtypes" )
	public void saveAll( Collection c);
	
	public static class Locator
	{
		static Context ctx;
		static {
			try {
				ctx = new InitialContext();
			} catch ( NamingException e ) {
				throw new RuntimeException( "SoruCevapService not found!", e );
			}
		}
		
		public static SoruCevapService getService()
		{
			
			try {
				return (SoruCevapService) ctx.lookup( SoruCevapService.RemoteJNDIName );
			} catch ( NamingException e ) {
				throw new RuntimeException( "BlackPearlEJBService not found!", e );
			}
		}
		
	}
	
	public User authenticate( String email, String pwd, int idgroup, String token);
	
	public void logout( String token);
	
	public List<Question> getQuestions( int idcategory);
	
	public List<Answer> getAnswers( int idquestion);
	
	public Question saveQuestion( String token, int idcategory, String question);
	
	public Answer saveAnswer( String token, int idquestion, String answer);
	
	public List<Category> getCategories();
	
	public Category saveCategory( String token, String name);
}
