package com.sorucevap.srv;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.log4j.Logger;

import com.sorucevap.srv.entity.Answer;
import com.sorucevap.srv.entity.Category;
import com.sorucevap.srv.entity.Question;
import com.sorucevap.srv.entity.User;

@SuppressWarnings( "unchecked" )
public @Stateless
class SoruCevapServiceBean implements SoruCevapService
{
	@SuppressWarnings( "unused" )
	private static Logger logger = Logger.getLogger( SoruCevapService.class );
	
	@PersistenceContext
	protected EntityManager em;
	
	public void runNativeSQL( String sql)
	{
		em.createNativeQuery( sql ).executeUpdate();
	}
	
	public void runNativeSQL( String sql, Object[] params)
	{
		
		Query query = em.createNativeQuery( sql );
		for ( int i = 0; i < params.length; i++ ) {
			query.setParameter( i, params[i] );
		}
		query.executeUpdate();
	}
	
	public <T> T save( T o)
	{
		
		if ( o == null ) {
			return null;
		}
		o = em.merge( o );
		em.persist( o );
		
		return o;
	}
	
	@SuppressWarnings( "rawtypes" )
	public void saveAll( Collection c)
	{
		
		for ( Object o : c ) {
			save( o );
		}
	}
	
	public void remove( Object o)
	{
		
		if ( o == null ) {
			return;
		}
		em.remove( o );
	}
	
	public Object refresh( Object o)
	{
		
		if ( o == null ) {
			return null;
		}
		em.refresh( o );
		return o;
	}
	
	public <T> List<T> query( String s)
	{
		
		return em.createQuery( s ).getResultList();
	}
	
	public <T> List<T> query( String s, Object[] params)
	{
		
		Query query = em.createQuery( s );
		for ( int i = 0; i < params.length; i++ ) {
			query.setParameter( i, params[i] );
		}
		return query.getResultList();
	}
	
	// ////////////////////////////////////////////////////////////////////////////////////////
	
	@Override
	public User authenticate( String email, String pwd, int idgroup, String token)
	{
		List<User> list = query(
		        "from User u where u.email=?0 and u.password=?1 and u.group.idgroup=?2 and u.deleted=0", new Object[] {
		                email, pwd, idgroup } );
		if ( list.size() > 0 ) {
			User user = list.get( 0 );
			user.setToken( token );
			return user;
		}
		return null;
	}
	
	@Override
	public void logout( String token)
	{
		List<User> list = query( "from User u where u.token=?0", new Object[] { token } );
		if ( list.size() > 0 ) {
			User user = list.get( 0 );
			user.setToken( null );
		}
	}
	
	@Override
	public List<Question> getQuestions( int idcategory)
	{
		List<Question> list = null;
		if ( idcategory > 0 ) {// belli bir derse ait sorular
			list = query( "from Question q where  q.deleted=0 and q.category.idcategory=" + idcategory
			        + " order by date DESC" );
		} else {// tum sorular
			list = query( "from Question q where q.deleted=0 order by date DESC" );
		}
		for ( Question q : list ) {
			q.getUser().getEmail();
			q.getCategory().getName();
		}
		return list;
	}
	
	@Override
	public List<Answer> getAnswers( int idquestion)
	{
		List<Answer> list = query( "from Answer a where a.question.idquestion=" + idquestion + " and a.deleted=0" );
		for ( Answer a : list ) {
			a.getUser().getEmail();
		}
		return list;
	}
	
	@Override
	public Question saveQuestion( String token, int idcategory, String question)
	{
		User user = getUserByToken( token );
		Category category = getCategory( idcategory );
		if ( user == null || category == null ) return null;
		Question ques = new Question( user, category, "", question, new Date(), false );
		ques = save( ques );
		return ques;
	}
	
	private Category getCategory( int idcategory)
	{
		List<Category> list = query( "from Category c where c.idcategory=" + idcategory );
		if ( list.size() > 0 ) return list.get( 0 );
		return null;
	}
	
	private User getUserByToken( String token)
	{
		List<User> list = query( "from User u where u.token=?0", new Object[] { token } );
		if ( list.size() > 0 ) return list.get( 0 );
		return null;
	}
	
	@Override
	public Answer saveAnswer( String token, int idquestion, String answer)
	{
		User user = getUserByToken( token );
		Question question = getQuestion( idquestion );
		if ( user == null || question == null ) return null;
		Answer ans = new Answer( user, question, answer, new Date(), false );
		ans = save( ans );
		return ans;
	}
	
	private Question getQuestion( int idquestion)
	{
		List<Question> list = query( "from Question q where q.idquestion=" + idquestion );
		if ( list.size() > 0 ) return list.get( 0 );
		return null;
	}
	
	@Override
	public List<Category> getCategories()
	{
		return query( "from Category c where c.deleted=0" );
	}
	
	@Override
	public Category saveCategory( String token, String name)
	{
		User user = getUserByToken( token );
		if ( user == null ) return null;
		Category cat = new Category( user, name, new Date(), false );
		cat = save( cat );
		return cat;
	}
}
